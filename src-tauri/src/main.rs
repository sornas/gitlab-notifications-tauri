// Prevents additional console window on Windows in release, DO NOT REMOVE!!
#![cfg_attr(not(debug_assertions), windows_subsystem = "windows")]

use std::collections::BTreeMap;

use api::{issue_activity, mr_activity};
use chrono::{DateTime, NaiveDate, NaiveDateTime, NaiveTime, Utc};
use directories::ProjectDirs;
use model::{IssueActivity, MrActivity};
use serde::{Deserialize, Serialize};
use tauri::async_runtime::RwLock as AsyncRwLock;
use tokio::task::JoinSet;
use tracing::debug;
use util::hash_u64;

use crate::model::{IssueActivityKind, MrActivityKind};

mod api;
mod model;
mod util;

type ManagedState<'a> = tauri::State<'a, AsyncRwLock<(State, InternalState)>>;

#[derive(Clone, Debug, Deserialize, Serialize)]
struct Entry {
    hash: String,
    title: String,
    repository: String,
    // TODO: include time so sort is actually sorted. only display date in UI
    date: String,
    body: Option<String>,
    url: Option<String>,
}

impl Entry {
    fn from_issue_activity(issue: &IssueActivity) -> Self {
        let IssueActivity {
            repository,
            issue_id,
            issue_title,
            date,
            url,
            kind,
        } = issue;
        let hash = hash_u64(issue).to_string();
        match kind {
            IssueActivityKind::Comment { author, body } => Entry {
                hash,
                title: format!(
                    "{} wrote a comment on #{} ({})",
                    author, issue_id, issue_title
                ),
                repository: repository.to_string(),
                date: date.date_naive().to_string(),
                body: Some(body.clone()),
                url: Some(url.clone()),
            },
            IssueActivityKind::Created { author, body } => Entry {
                hash,
                title: format!("{} created issue #{}: {}", author, issue_id, issue_title),
                repository: repository.to_string(),
                date: date.date_naive().to_string(),
                body: Some(body.clone()),
                url: Some(url.clone()),
            },
        }
    }

    fn from_mr_activity(mr: &MrActivity) -> Self {
        let MrActivity {
            repository,
            mr_id,
            mr_title,
            date,
            url,
            kind,
        } = mr;
        let hash = hash_u64(mr).to_string();
        match kind {
            MrActivityKind::Comment { author, body } => Entry {
                hash,
                title: format!("{} wrote a comment on !{} ({})", author, mr_id, mr_title),
                repository: repository.to_string(),
                date: date.date_naive().to_string(),
                body: Some(body.clone()),
                url: url.clone(),
            },
            MrActivityKind::Created { author, body } => Entry {
                hash,
                title: format!(
                    "{} created MR !{}: {}",
                    author.as_deref().unwrap_or("<unknown user>"),
                    mr_id,
                    mr_title
                ),
                repository: repository.to_string(),
                date: date.date_naive().to_string(),
                body: Some(body.clone()),
                url: url.clone(),
            },
            MrActivityKind::Merged {
                author,
                target_branch,
            } => Entry {
                hash,
                title: format!(
                    "{} merged MR !{} into {}",
                    author.as_deref().unwrap_or("<unknown user>"),
                    mr_id,
                    target_branch
                ),
                repository: repository.to_string(),
                date: date.date_naive().to_string(),
                body: None,
                url: url.clone(),
            },
        }
    }
}

#[derive(Default, Deserialize, Serialize)]
struct State {
    last_checked: BTreeMap<String, DateTime<Utc>>,
    entries: Vec<Entry>,
}

struct InternalState {
    access_token: String,
}

impl State {
    fn add_issues_from_api(
        &mut self,
        issues: &[issue_activity::response::Issue],
        since: DateTime<Utc>,
    ) {
        let activity = issues.iter().flat_map(|issue| {
            let notes = issue.notes.iter().flat_map(|note| {
                use issue_activity::response::NoteKind;
                let kind = match &note.kind {
                    NoteKind::Comment { body } => IssueActivityKind::Comment {
                        author: note.author.clone(),
                        body: body.clone(),
                    },
                    // Ignored activity:
                    NoteKind::UpdateDescription => return None,
                    // TODO: warning?
                    NoteKind::UnknownSystemNote { action: _, body: _ } => return None,
                };
                (note.created_at >= since).then_some(IssueActivity {
                    repository: issue.repository.clone(),
                    issue_id: issue.id.clone(),
                    issue_title: issue.title.clone(),
                    date: note.created_at,
                    url: note.url.clone(),
                    kind,
                })
            });
            let created = (issue.created_at >= since).then(|| IssueActivity {
                repository: issue.repository.clone(),
                issue_id: issue.id.clone(),
                issue_title: issue.title.clone(),
                date: issue.created_at,
                url: issue.url.clone(),
                kind: IssueActivityKind::Created {
                    author: issue.author.clone(),
                    body: issue.body.clone(),
                },
            });
            notes.chain(created)
        });
        self.entries
            .extend(activity.map(|activity| Entry::from_issue_activity(&activity)));
        self.entries.sort_by(|e1, e2| e1.hash.cmp(&e2.hash));
        self.entries.dedup_by(|e1, e2| e1.hash.eq(&e2.hash));
    }

    fn add_mrs_from_api(&mut self, mrs: &[mr_activity::response::Mr], since: DateTime<Utc>) {
        let activity = mrs.iter().flat_map(|mr| {
            let notes = mr.notes.iter().flat_map(|note| {
                use mr_activity::response::NoteKind;
                let kind = match &note.kind {
                    NoteKind::Comment { body } => MrActivityKind::Comment {
                        author: note.author.clone(),
                        body: body.clone(),
                    },
                    // Ignored activity:
                    NoteKind::UpdateDescription => return None,
                    // TODO: warning?
                    NoteKind::UnknownSystemNote { action: _, body: _ } => return None,
                };
                (note.created_at >= since).then_some(MrActivity {
                    repository: mr.repository.clone(),
                    mr_id: mr.id.clone(),
                    mr_title: mr.title.clone(),
                    date: note.created_at,
                    url: note.url.clone(),
                    kind,
                })
            });
            let created = (mr.created_at >= since).then(|| MrActivity {
                repository: mr.repository.clone(),
                mr_id: mr.id.clone(),
                mr_title: mr.title.clone(),
                date: mr.created_at,
                url: mr.url.clone(),
                kind: MrActivityKind::Created {
                    author: mr.author.clone(),
                    body: mr.body.clone(),
                },
            });
            notes.chain(created)
        });
        self.entries
            .extend(activity.map(|activity| Entry::from_mr_activity(&activity)));
        self.entries.sort_by(|e1, e2| e1.hash.cmp(&e2.hash));
        self.entries.dedup_by(|e1, e2| e1.hash.eq(&e2.hash));
    }

    fn read() -> Option<Self> {
        let s = std::fs::read_to_string(project_dirs().data_dir().join("store.json")).ok()?;
        serde_json::from_str(&s).ok()
    }

    fn write(&mut self) {
        let path = project_dirs().data_dir().join("store.json");
        if let Some(parent) = path.parent() {
            std::fs::create_dir_all(parent).unwrap();
        }
        std::fs::write(path, serde_json::to_string(self).unwrap()).unwrap();
    }
}

fn project_dirs() -> ProjectDirs {
    // TODO: handle None?
    ProjectDirs::from("", "", "gitlab-notifications").unwrap()
}

#[tauri::command]
async fn get_entries(state: ManagedState<'_>) -> Result<Vec<Entry>, String> {
    let (state, _) = &*state.read().await;
    Ok(state.entries.clone())
}

#[tauri::command]
async fn remove_entry(state: ManagedState<'_>, hash: String) -> Result<(), String> {
    let (state, _) = &mut *state.write().await;
    state.entries.retain(|e| e.hash != hash);
    state.write();
    Ok(())
}

fn parse_date(since: &str) -> Result<DateTime<Utc>, String> {
    let dt = NaiveDateTime::new(
        NaiveDate::parse_from_str(since, "%Y-%m-%d")
            .map_err(|e| format!("error parsing date: {e}"))?,
        NaiveTime::from_hms_opt(0, 0, 0).unwrap(),
    )
    .and_utc();
    Ok(dt)
}

#[tauri::command]
async fn update_repository(
    state: ManagedState<'_>,
    repository: String,
    since: Option<String>,
) -> Result<(), String> {
    let since = since.as_deref().map(parse_date).transpose()?;
    let (state, internal) = &mut *state.write().await;
    use std::collections::btree_map::Entry;
    let last_checked = state.last_checked.entry(repository.clone());
    let since = match (last_checked, since) {
        (Entry::Vacant(_), None) => todo!("error"),
        (Entry::Vacant(_), Some(since)) => since,
        (Entry::Occupied(o), None) => *o.get(),
        (Entry::Occupied(o), Some(since)) => (*o.get()).min(since),
    };
    state
        .last_checked
        .insert(repository.clone(), chrono::Utc::now());

    let issues = {
        let access_token = internal.access_token.clone();
        let repository = repository.clone();
        async move {
            issue_activity::Query::get(&access_token, repository, since)
                .await
                .map_err(|e| format!("error updating issues: {e}"))
        }
    };
    let mrs = {
        let access_token = internal.access_token.clone();
        async move {
            mr_activity::Query::get(&access_token, repository, since)
                .await
                .map_err(|e| format!("error updating issues: {e}"))
        }
    };
    let (issues, mrs) = tokio::join!(issues, mrs);
    state.add_issues_from_api(&issues?, since);
    state.add_mrs_from_api(&mrs?, since);
    state.write();
    Ok(())
}

#[tauri::command]
async fn update_all_repositories(
    state: ManagedState<'_>,
    since: Option<String>,
) -> Result<(), String> {
    let (state, internal) = &mut *state.write().await;
    let since = since.as_deref().map(parse_date).transpose()?;
    // time to spawn some tasks
    let mut issue_tasks = JoinSet::new();
    let mut mr_tasks = JoinSet::new();
    let now = chrono::Utc::now();
    for (repository, last_checked) in &mut state.last_checked {
        {
            let access_token = internal.access_token.clone();
            let repository = repository.clone();
            let since = since.unwrap_or(*last_checked).min(*last_checked);
            issue_tasks.spawn(async move {
                debug!("querying {} for issue activity since {}", repository, since);
                (
                    issue_activity::Query::get(&access_token, repository, since)
                        .await
                        .map_err(|e| format!("error updating issues: {e}")),
                    since,
                )
            });
        }
        {
            let access_token = internal.access_token.clone();
            let repository = repository.clone();
            let since = since.unwrap_or(*last_checked).min(*last_checked);
            mr_tasks.spawn(async move {
                debug!("querying {} for mr activity since {}", repository, since);
                (
                    mr_activity::Query::get(&access_token, repository, since)
                        .await
                        .map_err(|e| format!("error updating issues: {e}")),
                    since,
                )
            });
        }
        *last_checked = now;
    }
    while let Some(resp) = issue_tasks.join_next().await {
        let (issues, since) = resp.map_err(|e| e.to_string())?;
        state.add_issues_from_api(&issues?, since);
    }
    while let Some(resp) = mr_tasks.join_next().await {
        let (mrs, since) = resp.map_err(|e| e.to_string())?;
        state.add_mrs_from_api(&mrs?, since);
    }
    state.write();
    Ok(())
}

fn main() {
    tracing_subscriber::fmt::init();
    tracing::error!("logging errors");
    tracing::warn!("logging warnings");
    tracing::info!("logging info");
    tracing::debug!("logging debugs");
    tracing::trace!("logging traces");
    let access_token_file = project_dirs().config_dir().join("access-token");
    if !access_token_file.exists() {
        // TODO show error in UI
        println!(
            "expected gitlab access-token at {}",
            access_token_file.display()
        );
        return;
    }
    let access_token = std::fs::read_to_string(access_token_file)
        .unwrap()
        .trim()
        .to_string();
    let mut state = State::read().unwrap_or_default();
    let projects_file = project_dirs().config_dir().join("repositories");
    if projects_file.exists() {
        for project in std::fs::read_to_string(projects_file).unwrap().lines() {
            state.last_checked.insert(project.to_string(), Utc::now());
        }
    }
    tauri::Builder::default()
        .manage(AsyncRwLock::new((state, InternalState { access_token })))
        .invoke_handler(tauri::generate_handler![
            get_entries,
            remove_entry,
            update_all_repositories,
            update_repository,
        ])
        .run(tauri::generate_context!())
        .expect("error while running tauri application");
}

use chrono::{DateTime, Utc};
use serde::{Deserialize, Serialize};

#[derive(Deserialize, Serialize, PartialEq, Eq, Hash)]
pub struct IssueActivity {
    pub repository: String,
    pub issue_id: String,
    pub issue_title: String,
    pub date: DateTime<Utc>,
    pub url: String,
    pub kind: IssueActivityKind,
}

#[derive(Deserialize, Serialize, PartialEq, PartialOrd, Eq, Ord, Hash)]
pub enum IssueActivityKind {
    Comment { author: String, body: String },
    Created { author: String, body: String },
}

#[derive(Deserialize, Serialize, PartialEq, Eq, Hash)]
pub struct MrActivity {
    pub repository: String,
    pub mr_id: String,
    pub mr_title: String,
    pub date: DateTime<Utc>,
    pub url: Option<String>,
    pub kind: MrActivityKind,
}

#[derive(Deserialize, Serialize, PartialEq, PartialOrd, Eq, Ord, Hash)]
pub enum MrActivityKind {
    Comment {
        author: String,
        body: String,
    },
    Created {
        author: Option<String>,
        body: String,
    },
    Merged {
        author: Option<String>,
        target_branch: String,
    },
}
